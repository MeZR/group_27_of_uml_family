# TensorFlow Android Camera Demo

Tensorflow object detection API 物体检测手机APP
# 组成员
        孟昭瑞  201601450201
        伊桐杰  201601450205
        韩韶波  201601450206
        郭文康  201601450217
        孙洪福  201601450222
        吴延赛  201601450225
        闫磊    201601450226
# 组成员贡献
        孟昭瑞:
                下载编译成功源程序
                UML协作图
        伊桐杰:
                下载编译成功源程序
                UML部署图
        韩韶波:
                下载编译成功源程序
                UML构件图
        郭文康:
                下载编译成功源程序
                UML顺序图
        孙洪福:
                下载编译成功源程序
                UML用例图
        吴延赛:
                下载编译成功源程序
                UML活动图
        闫磊:
                下载编译成功源程序
                UML状态图


# 组员共同任务
        1. 在github下载源码,地址:https://github.com/tensorflow/tensorflow.git
        2. 将android项目解压出来并导入android studio 或 idea
        3. 安装并配置java 1.8 环境
        4. 下载并配置android sdk
        5. 编译并修改错误
        6. 编译成功后将debug文件夹下的apk安装到手机上
        7. 在手机上测试程序








此文件夹包含一个示例应用程序，该应用程序在Android设备上使用TensorFlow。

## 说明

此文件夹中的演示旨在提供在移动应用程序中使用TensorFlow的简单示例。


推理是使用TensorFlowandroid推理接口完成的，如果您希望将一个独立的库放入现有的应用程序中，可以单独构建该接口。对象跟踪和有效的yuv->rgb转换由libtensorflow.so处理。


由于使用了camera2 api，运行android 5.0（api 21）或更高版本的设备需要运行演示，尽管本机库本身可以在api>=14设备上运行。


## 当前样本：

1. [TF Classify](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/android/src/org/tensorflow/demo/ClassifierActivity.java):
        tf classify：使用google inception模型实时对摄像机帧进行分类，在摄像机图像上显示叠加的顶部结果。
2. [TF Detect](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/android/src/org/tensorflow/demo/DetectorActivity.java):
        tf detect：演示一个SSD mobilenet模型，该模型使用
        [Tensorflow Object Detection API](https://github.com/tensorflow/models/tree/master/research/object_detection/)
        TensorFlow对象检测API进行培训， 该API引入了[Speed/accuracy trade-offs for modern convolutional object detectors](https://arxiv.org/abs/1611.10012) 现代卷积对象检测器的速度/精度权衡，以实时定位和跟踪相机预览中的对象（80个类别）。
3. [TF Stylize](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/android/src/org/tensorflow/demo/StylizeActivity.java):
        tf-styleze：使用基于艺术风格的学习表示的模型，将相机预览图像重新设置为多个不同艺术家的图像。
4.  [TF Speech](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/android/src/org/tensorflow/demo/SpeechActivity.java):
    运行由 [audio trainingtutorial](https://www.tensorflow.org/versions/master/tutorials/audio_recognition)音频训练教程构建的简单语音识别模型。侦听一小组单词，并在识别时在UI中突出显示它们。

<img src="sample_images/classify1.jpg" width="30%"><img src="sample_images/stylize1.jpg" width="30%"><img src="sample_images/detect1.jpg" width="30%">


## 预建组件:

如果您只想要尝试演示的最快路径，可以在
[here](https://ci.tensorflow.org/view/Nightly/job/nightly-android/). 此处下载每晚构建 。展开“View”，然后展开“Last Successful Artifacts”下的“out”文件夹，找到tensorflow_demo.apk。

另外还有预编译的本机库和一个jcenter包，您可以将其放入自己的应用程序中。有关 详细信息，请参阅
[tensorflow/contrib/android/README.md](../../../tensorflow/contrib/android/README.md)

## 运行演示

安装应用程序后，可以通过“TF Classify”，“TF Detect”，“TF Stylize”和“TF Speech”图标启动它们，这些图标的橙色TensorFlow标志作为其图标。

在运行活动时，按设备上的音量键将打开/关闭调试可视化，向屏幕呈现可能对开发有用的其他信息。

## 使用JCenter中的TensorFlow AAR在Android Studio中构建

自己编译演示应用程序并尝试更改项目代码的最简单方法是使用AndroidStudio。只需将此android目录设置为项目根目录即可。

然后编辑build.gradle文件并更改nativeBuildSystemto 的值，'none'以便以最简单的方式构建项目：

def nativeBuildSystem ='none'
虽然此项目包括TensorFlow的完整构建集成，但此设置禁用它，并使用JCenter的TensorFlow推理接口包。

注意：目前，在此构建模式下，YUV - > RGB使用效率较低的Java实现完成，并且“TF Detect”活动中没有对象跟踪。将构建系统设置为'cmake'当前仅构建 libtensorflow_demo.so，提供快速YUV - > RGB转换和对象跟踪，同时仍通过下载的AAR获取TensorFlow支持，因此它可能是启用这些功能的轻量级方法。

对于任何不包含自定义低级TensorFlow代码的项目，这可能就足够了。

有关如何在您自己的项目中包含此JCenter包的详细信息，请参阅
[tensorflow/contrib/android/README.md](../../../tensorflow/contrib/android/README.md)

## 使用来自Source的TensorFlow构建演示

选择下面的首选方法。目前，我们完全支持Bazel，并且对gradle，cmake，make和Android Studio提供部分支持。

作为所有构建类型的第一步，克隆TensorFlow仓库：

git clone --recurse-submodules https://github.com/tensorflow/tensorflow.git
请注意，这--recurse-submodules是防止protobuf编译的一些问题所必需的。
### Bazel

注意：Bazel目前不支持在Windows上构建Android。对gradle / cmake构建的全面支持即将推出，但与此同时我们建议Windows用户下载预构建的二进制文件。
[prebuilt binaries](https://ci.tensorflow.org/view/Nightly/job/nightly-android/) instead.

##### 安装Bazel和Android先决条件

Bazel是TensorFlow的主要构建系统。要使用Bazel构建，必须在您的系统上安装它和Android NDK和SDK。



1.  按照Bazel网站上的说明安装最新版本的Bazel [on the Bazel website](https://bazel.build/versions/master/docs/install.html).
2.  Android NDK是构建本机（C / C ++）TensorFlow代码所必需的。目前推荐的版本是14b，可在此处找到 。
    [here](https://developer.android.com/ndk/downloads/older_releases.html#ndk-14b-downloads).

      NDK 16，即2017年11月发布的修订版，与Bazel 不兼容。看到这里。[here](https://github.com/tensorflow/tensorflow/issues/14918).

3.  Android SDK和构建工具可以在此处获得，
    [here](https://developer.android.com/tools/revisions/build-tools.html),也可以作为Android Studio的一部分获取 。
    [Android Studio](https://developer.android.com/studio/index.html). 构建工具API> = 23是构建TF Android演示所必需的（尽管它将在API> = 21设备上运行）。

      Android Studio SDK Manager的NDK安装程序将安装最新版本的NDK，这与Bazel 不兼容。您需要手动下载旧版本，如（2）所示。
##### 编辑WORKSPACE

注意：只要安装了SDK和NDK，./configure脚本就会为您创建这些规则。当脚本要求自动配置时，回答“是” ./WORKSPACE。

<workspace_root>/WORKSPACE必须取消注释Android条目， 并根据NDK和SDK的安装位置适当填充路径。否则会报告错误，例如：“外部标签'//外部：android / sdk'没有绑定任何东西”将被报告。

同时将WORKSPACE中SDK的API级别编辑为SDK中安装的最高级别。这必须> = 23（这完全独立于演示的API级别，该级别在AndroidManifest.xml中定义）。NDK API级别可能保持在14。

##### 安装模型文件（可选)

GraphDef包含模型定义和权重的TensorFlow 由于其大小而未打包在repo中。它们会自动下载并通过Bazel通过WORKSPACE在构建过程中定义的new_http_archive 以及Gradle via download-models.gradle 与APK一起打包。

可选：如果您希望手动将模型放在资源中，请model_files从文件assets中tensorflow_demo 找到的列表中删除所有条目BUILD。然后自己下载并解压缩到assets源树中的目录：

```bash
BASE_URL=https://storage.googleapis.com/download.tensorflow.org/models
for MODEL_ZIP in inception5h.zip ssd_mobilenet_v1_android_export.zip stylize_v1.zip
do
  curl -L ${BASE_URL}/${MODEL_ZIP} -o /tmp/${MODEL_ZIP}
  unzip /tmp/${MODEL_ZIP} -d tensorflow/examples/android/assets/
done
```

这会将模型及其关联的元数据文件提取到本地assets /目录。

如果您使用的是Gradle，请确保在手动下载模型后从build.gradle中删除download-models.gradle引用; 否则gradle可能会再次下载模型并覆盖您的模型。

##### 建立

确保在Android 5.0（API 21）或更高版本的设备上启用了adb调试，然后在构建中使用工作区根目录中的以下命令来安装APK：

```bash
bazel build -c opt //tensorflow/examples/android:tensorflow_demo
```

##### 安装

确保在Android 5.0（API 21）或更高版本的设备上启用了adb调试，然后在构建中使用工作区根目录中的以下命令来安装APK：

```bash
adb install -r bazel-bin/tensorflow/examples/android/tensorflow_demo.apk
```

### 带有Bazel的Android Studio

Android Studio可用于与Bazel一起构建演示。首先，确保您可以按照上述说明使用Bazel进行构建。然后，查看build.gradle并确保Bazel的路径与系统的路径匹配。

此时，您可以将tensorflow / examples / android目录添加为新的Android Studio项目。点击安装它请求的所有Gradle扩展，您应该能够让Android Studio像任何其他应用程序一样构建演示（它将调用Bazel以使用NDK构建本机代码）。

### CMake

该演示的完整CMake支持即将推出，但目前可以使用
[tensorflow/contrib/android/cmake](../../../tensorflow/contrib/android/cmake)..